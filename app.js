const yargs =require('yargs');

const weather = require('./weather/weather');
const geocode = require('./geocode/geocode');

const argv = yargs
    .options({
    address: {
        describe: 'Address to fetch weather for',
        demand: true,
        alias: 'a',
        string: true
    }
})
    .help()
    .alias('help', 'h')
    .argv;


geocode.geocodeAddress(argv.a ,(err, results) => {
    if (err) {
        console.log(err);
    } else {
        console.log(JSON.stringify(results, undefined, 2));
        weather.getWeather(results.latitude,results.longtitude, (err, weatherResults) => {
            if(err) {
                console.log(err);
            } else {
                console.log(`It's currently ${weatherResults.temperature}. It feels like ${weatherResults.apparentTemperature}`);
            }
        });
    }
});



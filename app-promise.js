const yargs =require('yargs');
const axios = require('axios');


const argv = yargs
    .options({
        address: {
            describe: 'Address to fetch weather for',
            demand: true,
            alias: 'a',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

const encodedAddress = encodeURIComponent(argv.a);

const geocodeUrl =`http://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

axios.get(geocodeUrl).then( (response) => {
    if (response.data.status === 'ZERO_RESULTS') {
        throw new Error('Unable to find this address.');
    }
    const lat = response.data.results[0].geometry.location.lat;
    const lng = response.data.results[0].geometry.location.lng;
    const weatherUrl =`https://api.forecast.io/forecast/36e9a6b4bb0088378eed3a83d08939e5/${lat},${lng}`;
    console.log(response.data);
    return axios.get(weatherUrl);
})
    .then((res) => {
        const temperature = res.data.currently.temperature;
        const apparentTemperature = res.data.currently.apparentTemperature;
        console.log(`Its currently ${temperature}. It feels like ${apparentTemperature}`);
    })
    .catch( err => {
    if (err.code === 'ENOTFOUND') {
        console.log('Unable to connect to server API');
    } else {
        console.log(err);
    }
});






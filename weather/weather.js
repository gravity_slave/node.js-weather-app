const request = require('request');

const getWeather = (lat,lng, callback) => {
    request({
        url: `https://api.forecast.io/forecast/36e9a6b4bb0088378eed3a83d08939e5/${lat},${lng}`,
        json: true
    }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        } else {
            callback('Unable to fetch weather.');
        }
    });
};



module.exports.getWeather  = getWeather;
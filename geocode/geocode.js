const request = require('request');

const geocodeAddress = (address, clbk) => {
    const url =encodeURIComponent(address);

request({
    url: `http://maps.googleapis.com/maps/api/geocode/json?address=${url}`,
    json: true
}, (err,res, body) => {
    if(err) {
        clbk('Unable to connect to Google servers');

    } else if (body.status === 'ZERO_RESULTS') {
        clbk('UNABLE to find that address');
    } else if (body.status === 'OK') {
        clbk(undefined, {
            address:body.results[0].formatted_address,
            latitude: body.results[0].geometry.location.lat,
            longtitude: body.results[0].geometry.location.lng
        });

    }
});

};

module.exports = {
    geocodeAddress
};